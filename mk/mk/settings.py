# -*- coding: utf-8 -*-

# Scrapy settings for mk project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#     http://scrapy.readthedocs.org/en/latest/topics/downloader-middleware.html
#     http://scrapy.readthedocs.org/en/latest/topics/spider-middleware.html

BOT_NAME = 'mk'

SPIDER_MODULES = ['mk.spiders']
NEWSPIDER_MODULE = 'mk.spiders'


# Crawl responsibly by identifying yourself (and your website) on the user-agent
USER_AGENT = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.79 Safari/537.36'

# Obey robots.txt rules
ROBOTSTXT_OBEY = False
CONCURRENT_REQUESTS = 2
DOWNLOAD_DELAY = 0.7


FEED_EXPORTERS = {
    'csv': 'mk.csv_item_exporter.MyProjectCsvItemExporter',
}

CSV_DELIMITER = "|"
FIELDS_TO_EXPORT = ['productcode', 'altproductcode', 'gender', 'category1', 'category2', 'category3', 'brand', 'color1', 'color2', 'color3', 'color4', 'color5', 'color6', 'color7', 'color8', 'color9', 'color10', 'color11', 'color12', 'color13', 'color14', 'color15', 'color16', 'color17', 'color18', 'color19', 'color20', 'size1', 'size2', 'size3', 'size4', 'size5', 'size6', 'size7', 'size8', 'size9', 'size10', 'size11', 'size12', 'size13', 'size14', 'size15', 'size16', 'size17', 'size18', 'size19', 'size20', 'size21', 'size22', 'size23', 'size24', 'size25', 'size26', 'size27', 'size28', 'size29', 'size30', 'composition', 'details', 'measures', 'price', 'fullprice', 'min_price', 'max_price', 'currency', 'country', 'isavailable', 'stock', 'itemurl', 'imageurl', 'title']
