# -*- coding: utf-8 -*-
from scrapy import Request, Spider
from scrapy.loader import ItemLoader
from mk.items import MkItem

class MichaelkorsSpider(Spider):
    name = "michaelkors"
    allowed_domains = ["michaelkors.com", "michaelkors.ca"]

    start_urls = (
        'http://www.michaelkors.com/browse/search/lazyLoading.jsp?No={}&rlPath=content/web/share/category/Result Lists/Default Results List',
        'http://www.michaelkors.ca/browse/search/lazyLoading.jsp?No={}&rlPath=content/web/share/category/Result Lists/Default Results List',
    )

    def start_requests(self):
        for number in range(0, 3000, 42):
            for url in self.start_urls:
                url = url.format(number)
                yield Request(url)

    def parse(self, response):
        links = response.xpath('//div[@class="product_panel"]/a[@oncontextmenu]/@href').extract()
        links = [response.urljoin(link) for link in links]
        for link in links:
            yield Request(link, self.parse_product)

    def parse_product(self, response):
        strip_empty = lambda x: x[0].strip().replace('\n', '') if x else ''
        item = MkItem()
        item['productcode'] =  response.xpath(
            '//script[contains(text(), "mfrItemNum")]').re('\"mfrItemNum\" *: *\"(.+)\"')
        item['altproductcode'] = response.xpath(
            '//script[contains(text(), "productID")]').re('\"productID\" *: *\"(.+)\"')
        item['category1'] = response.xpath(
            '//script[contains(text(), "categoryID")]').re('\"categoryID\" *: *\"(.+)\"')
        item['category2'] = response.xpath(
            '//script[contains(text(), "siteSectionLevel2")]').re('\"siteSectionLevel2\" *: *\"(.+)\"')
        item['category3'] = response.xpath(
            '//script[contains(text(), "siteSectionLevel3")]').re('\"siteSectionLevel3\" *: *\"(.+)\"')
        item['brand'] = response.xpath(
            '//script[contains(text(), "mfr")]').re('\"mfr\" *: *\"(.+)\"')
        try:
            brand = item.get('brand')[0]
        except IndexError:
            brand = ''
        if brand == 'Michael Kors Mens':
            item['gender'] = 'Men'
        else:
            item['gender'] = 'Women'

        colors = response.xpath(
            '//ul[@class="color_group_list color_swatch"]/li//img/@onclick').re("colorChangeOmni\('(\w+)'")
        for color_index, color_value in enumerate(colors[:20]):
            item['color{}'.format(color_index+1)] = color_value

        sizes = response.xpath('//select[@class="size_select"]/option/text()').extract()
        if sizes:
            sizes.remove('CHOOSE SIZE')
            sizes = [size.strip() for size in sizes]
            for size_index, size_value in enumerate(sizes[:30]):
                item['size{}'.format(size_index+1)] = size_value

        composition = response.xpath(
            '//div[@class="pdp_description_content jspScrollable pdp_description_tabs_2"]//text()').extract()
        composition = "".join([composition_item.strip().replace('\n', '') for composition_item in composition])
        item['composition'] = composition
        item['details'] = strip_empty(
            response.xpath('//p[@itemprop="description"]/text()').extract())
        price = response.xpath('(//span[@itemprop="price"]/text())[1]').re('\d{1,3}[,\.\d{3}]*\.?\d*')
        if price:
            price = price[0].replace(',', '')
            item['price'] = price
        fullprice = response.xpath('(//span[@class="was_price"]/text())[1]').re('\d{1,3}[,\.\d{3}]*\.?\d*')
        if fullprice:
            fullprice = fullprice[0].replace(',', '')
            item['fullprice'] = fullprice
        item['currency'] = response.xpath(
            '//script[contains(text(), "currency")]').re('\"currency\" *: *\"(.+)\"')
        item['country'] = response.xpath(
            '//script[contains(text(), "accountCountry")]').re('\"accountCountry\" *: *\"(.+)\"')
        item['isavailable'] = response.xpath(
            '//script[contains(text(), "availability")]').re('\"availability\" *: *\"(.+)\"')
        item['stock'] = response.xpath(
            '//script[contains(text(), "qtyAvailable")]').re('\"qtyAvailable\" *: *\"(.+)\"')
        item['itemurl'] = response.url
        item['imageurl'] = response.xpath('//meta[@property="og:image"]/@content').extract()
        item['title'] = strip_empty(response.xpath('//h1[@class="prod_name"][@itemprop="name"]/text()').extract())
        min_price = response.xpath('(//span[@itemprop="lowPrice"]/text())[1]').re('\d{1,3}[,\.\d{3}]*\.?\d*')
        if min_price:
            min_price = min_price[0].replace(',', '')
            item['min_price'] = min_price
        max_price = response.xpath('(//span[@itemprop="highPrice"]/text())[1]').re('\d{1,3}[,\.\d{3}]*\.?\d*')
        if max_price:
            max_price = max_price[0].replace(',', '')
            item['max_price'] = max_price
        yield item
